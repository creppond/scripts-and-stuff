#!/usr/bin/env bash
#
# This script populates the ansible-cmdb host data, creates the ansible-cmdb 
# html file, and moves it to the web directory.
#
# Author: Corey Reppond <creppond@proton.me>

REPO=my-ansible-repo
CMDB_DIR=$HOME/$REPO/cmdb
HTML_FILE="overview.html"
WEB_DIR="/var/www/html/cmdb"

# handle errors in a uniform manner
err() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ERROR: $*" >&2
}

# if ansible doesn't exist, exit
[[ -z $(command -v ansible) ]] && err "ansible command unavailable" && exit 1

# if ansible-cmdb doesn't exist, exit
[[ -z $(command -v ansible-cmdb) ]] && err "ansible-cmdb command unavailable" && exit 1

# populate the host data
ansible -b -i "$CMDB_DIR"/hosts -m setup --tree "$CMDB_DIR"/out all > /dev/null 2>&1

# back up existing html file, if it exists
[[ -f "$CMDB_DIR"/"$HTML_FILE" ]] && \
    cp "$CMDB_DIR"/"$HTML_FILE"{,."$(date +%Y-%m-%d@%T)"} \
    || err "could not back up html file"

# create the updated html file
ansible-cmdb "$CMDB_DIR"/out "$CMDB_DIR"/out_custom >"$CMDB_DIR"/"$HTML_FILE" 

# copy / overwrite the web server html file
cp "$CMDB_DIR"/"$HTML_FILE" "$WEB_DIR"/"$HTML_FILE"

exit 0

# EOF
