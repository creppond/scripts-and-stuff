#!/usr/bin/env bash
#
# Perform a snapshot or full backup on a KVM domain (virtual machine).
#
# Author: Corey Reppond <creppond@proton.me>

# set defaults
vm_shut=0 # do not shut down the domain (vm) during backup

# print help
print_help()
{
cat <<END
Back up a KVM domain (virtual machine)

Usage: $0 [-b backupdir|-h|-n domain|s]

Options:
  -h      Display this help message
  -n      Domain name to back up
  -s      Shut the domain down prior to snapshot

END
    exit 0
}

# create a snapshot
do_backup() {
    local vm_running=1 # 1 = running, 0 = shut down

    # if the user selected to shut down the host, do that.
    if [[ $vm_shut == 1 ]]; then
        virsh shutdown "$vm_name"
        # wait for the vm to shut down, then proceed
        while [ $vm_running == 1 ]; do
            [[ -z $(virsh list |grep -i "$vm_name") ]] && vm_running=0
        done
    fi

    # create the back up
    virsh snapshot-create-as --domain "$vm_name" --name "$vm_name"-"$(date +%Y-%m-%d@%T)"

    # if the system is shut down, start it back up
    [[ -z $(virsh list |grep -i "$vm_name") ]] && virsh start "$vm_name"

    exit 0
}

# handle errors in a uniform manner
err() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ERROR: $*" >&2
}

# get command line options
while getopts ":hn:s" option; do
    case "${option}" in
        h)  print_help ;;
        n)  vm_name="${OPTARG}" ;;
        s)  vm_shut=1 ;;
        \?) echo "Invalid option: -$OPTARG" 1>&2 && exit 1;;
    esac
 done

# remove options
shift $((OPTIND -1))

##########################
#
# Check requirements
#
##########################

# need sudo
[[ $(whoami) != "root" ]] && err "root permissions required" && exit 1

# if the virsh command doesn't exist, exit
[[ -z $(command -v virsh) ]] && err "virsh command unavailable" && exit 1

# domain (virtual machine) is required
: "${vm_name:?Missing -m}"

# if the vm doesn't exist, exit
[[ -z $(virsh list --all |grep -i "$vm_name") ]] \
    && err "The virtual machine does not exist." && exit 1

##########################
#
# The actual work
#
##########################

# if not, do the backup
do_backup
