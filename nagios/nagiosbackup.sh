#!/usr/bin/env bash
#
# A quick and dirty script to back up the nagios config directory.
#
# Written by: Corey Reppond <creppond@proton.me>

NAGIOS_DIR="/etc/nagios"
TIMESTAMP=$(date +%Y-%m-%d-%H-%M-%S-%s)
BACKUP_PATH="/root/nagios_backups"
BACKUP_FILE="NagiosBackup-$TIMESTAMP.tar.gz"

tar -czvf $BACKUP_PATH/"$BACKUP_FILE" $NAGIOS_DIR

EXITVALUE=$?
if [ $EXITVALUE != 0 ]; then
    /usr/bin/logger -t nagiosbackup "ALERT exited abnormally with [$EXITVALUE]"
fi
exit 0
