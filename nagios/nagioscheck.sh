#!/bin/bash
#
# A script to keep me from having to type out a lot of things when making changes to nagios
# configs.
#
# Written by: Corey Reppond <creppond@proton.me>

NAGIOSBIN=$(which nagios)
NAGIOSCFG="/etc/nagios/nagios.cfg"

$NAGIOSBIN -v $NAGIOSCFG |grep "^Total"
